<?php $__env->startSection('navbar-brand'); ?>
    TO-DO
<?php $__env->stopSection(); ?>
<?php $__env->startSection('to-do-cont'); ?>
    <div class="container ">
        <?php if(count($tasks)>0): ?>
            <div class="container liste">
                <?php $__currentLoopData = $tasks; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $task): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="card-deck col-12" >
                        <div class="card m-2" style="border-left:2px solid red">
                            <div class="card-body row">
                                <h5 class="card-title col-11"><?php echo e($task->task); ?></h5>
                                <form action="<?php echo e(url ('task/'.$task->id)); ?>" method="POST">
                                    <button class="btn btn-outline-danger mt-">del</button>
                                    <?php echo e(csrf_field()); ?>

                                </form>
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php else: ?>
            <label class="text-center text-info" for="add" style="font-size: 40px">füge ein Task hinzu</label>
            <?php endif; ?> 
        </div>

        <form class="modal-content mt-2" action="<?php echo e(url('task')); ?>" method="POST">
            <div class="container">
                <div class="row">
                    <input type="text" class="col-9" maxlength="254" placeholder="dein Text hier" name="name" maxlength="14" required>
                    <input type="submit" id="add" class="btn btn-outline-primary col-3" value="hinzufügen"/>
                </div>
            </div>
            <?php echo e(csrf_field()); ?>

        </form>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /Applications/MAMP/htdocs/laravel/majd/resources/views/home.blade.php ENDPATH**/ ?>