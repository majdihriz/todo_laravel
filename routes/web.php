<?php
use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use function GuzzleHttp\Promise\task;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


Route::get('/home', function () {
    $tasks = Task::orderby('created_at', 'asc')->get();
    return view('home',[
        'tasks'=>$tasks,
    ]);
}); 

Route::post('/task', function (Request $request) {

    $task = new Task;
    $task->task = $request->name;
    $task->save();
    return redirect('/home');
});

Route::post('/task/{task}', function (Task $task) {
    $task->delete();
    return redirect('/home');
});